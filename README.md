# PRINCEY WEB #

Princey Web Provides UI to Princey API.

To run this app without firebase:
```
npm install
npm start
```

To run this app with local firebase functions:
```
npm run serve
```

It needs `firebase-tools` to be installed either globally or as dev dependency

to install firebase-tools globally
```
npm install -g firebase-tools
```

or as local dev dependency
```
npm install firebase-tools --save-dev
```

Please refer here: https://firebase.google.com/docs/functions/local-emulator to setup your env to run functions locally.
