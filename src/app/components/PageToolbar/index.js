import React, { Fragment } from 'react';
import makeStyles from '@material-ui/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Toolbar from '@material-ui/core/Toolbar';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(-3),
    marginBottom: theme.spacing(3),
    marginLeft: theme.spacing(-3),
    marginRight: theme.spacing(-3),
  },
  title: {
    marginRight: theme.spacing(6),
  },
  subtitle: {
    marginRight: theme.spacing(6),
  },
}));

export default function PageToolbar({ title, subtitle, children }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Toolbar>
        <Typography variant="h6" className={classes.title}>{title}</Typography>
        {subtitle && (<Typography variant="h6" className={classes.subtitle}>{subtitle}</Typography>)}
        {children}
      </Toolbar>
      <Divider />
    </div>
  );
}
