import React from 'react';
import ReactHtmlParser from 'react-html-parser';

export default function Html({ html }) {
  return <div>{ReactHtmlParser(html)}</div>
}
