import React from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  box: {
    textAlign: 'center',
    lineHeight: 1.2,
  },
  name: {
    color: '#131313',
    fontSize: 48,
    fontWeight: 'bold',
    marginRight: theme.spacing(3),
  },
  label: {
    color: '#FED541',
    fontSize: 48,
    fontWeight: 'bold',
  },
}));

export default function Brand({ name, label }) {
  const classes = useStyles();

  return (
    <Box className={classes.box}>
      <Typography className={classes.name} component="span">{name}</Typography>
      <Typography className={classes.label} component="span">{label}</Typography>
    </Box>
  );
}
