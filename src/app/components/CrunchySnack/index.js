import React, { Fragment, useState, useEffect } from 'react';
import { useSnackbar } from 'notistack';

export default function CrunchySnack({ error, callback = () => {} }) {
  const { enqueueSnackbar } = useSnackbar();

  const [errorCount, setErrorCount] = useState(0);

  const errorMessages = [
    'Rrrgh... Error! Don\'t worry and please drink some water!',
    'Error... Again? Please try again and again and again! (We\'re just trying to be funny!)',
    'Well, you can see the Refresh button, right? Don\'t hit it! It will fix the error',
  ];

  useEffect(() => {
    if (error) {
      enqueueSnackbar(errorMessages[errorCount], {
        variant: 'error',
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'center',
        },
      });

      callback(errorCount);
      const current = errorCount + 1;

      if (current > (errorMessages.length - 1)) {
        return setErrorCount(0);
      }

      return setErrorCount(current);

    }
  }, [error]);

  return <Fragment />
}
