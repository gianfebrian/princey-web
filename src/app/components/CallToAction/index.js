import React from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  box: {
    lineHeight: 1.2,
  },
  callToAction: {
    color: '#000000',
    textAlign: 'center',
    fontSize: 18,
  },
}));

export default function CallToAction({ children }) {
  const classes = useStyles();

  return (
    <Box className={classes.callToActionBox}>
      <Typography className={classes.callToAction}>
        {children}
      </Typography>
    </Box>
  );
}
