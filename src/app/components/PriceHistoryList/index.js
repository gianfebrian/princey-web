import React, { Fragment, useState } from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';

import TableHead from './../TableHead';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 200,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
}));

export default function PriceHistoryList({
  data,
  order,
  orderBy,
  page,
  rowsPerPage,
  onChangeOrder,
  onChangeSelection = () => { },
  onChangePage = () => {},
  onChangeRowsPerPage = () => {},
}) {
  const classes = useStyles();

  const [selected, setSelected] = useState([]);

  const handleSelectAll = e => {
    setSelected([]);

    let newSelecteds = [];
    if (e.target.checked) {
      newSelecteds = data.map(v => v.id);
      setSelected(newSelecteds);
    }

    (typeof onChangeSelection === 'function') && onChangeSelection(newSelecteds);
  }

  const handleRowClick = (e, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);

    (typeof onChangeSelection === 'function') && onChangeSelection(newSelected);
  };

  const isSelected = id => selected.indexOf(id) !== -1;
  const rows = data.rows || [];

  return (
    <Fragment>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table
            stickyHeader
            size="small"
            aria-label="price history list"
          >
            <TableHead
              headCells={[
                { id: 'timestamp', numeric: false, disablePadding: false, label: 'Update Time' },
                { id: 'price', numeric: false, disablePadding: false, label: 'Price' },
              ]}
              rowCount={data.totalCount}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAll={handleSelectAll}
              onChangeOrder={onChangeOrder}
            />

            <TableBody>
              {rows.map((row, i) => {
                const isItemSelected = isSelected(row._id);

                return (
                  <TableRow
                    hover
                    key={row._id}
                    onClick={e => handleRowClick(e, row._id)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    selected={isItemSelected}
                  >
                    <TableCell component="th" scope="row">
                      {new Date(row.timestamp).toLocaleString()}
                    </TableCell>
                    <TableCell align="left">
                      {(() => {
                        const formatted = (row.price || '').toLocaleString('en-ID', {
                          style: 'currency', currency: 'IDR'
                        });

                        return formatted;
                      })()}
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100]}
          component="div"
          count={data.totalCount || 0}
          rowsPerPage={rowsPerPage || 5}
          page={page || 0}
          onChangePage={onChangePage}
          onChangeRowsPerPage={onChangeRowsPerPage}
        />
      </Paper>
    </Fragment>
  );
}
