import React, { Fragment, useState } from 'react';
import Link from 'next/link';
import clsx from 'clsx';

import makeStyles from '@material-ui/core/styles/makeStyles';
import deepOrange from '@material-ui/core/colors/deepOrange';
import deepPurple from '@material-ui/core/colors/deepPurple';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TablePagination from '@material-ui/core/TablePagination';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';

import TableHead from './../TableHead';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  container: {},
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  },
  purple: {
    color: theme.palette.getContrastText(deepPurple[500]),
    backgroundColor: deepPurple[500],
  },
}));

export default function ProductList({
  data,
  order,
  orderBy,
  page,
  rowsPerPage,
  onChangeOrder,
  onChangeSelection = () => { },
  onChangePage,
  onChangeRowsPerPage,
}) {
  const classes = useStyles();

  const [selected, setSelected] = useState([]);

  const handleSelectAll = e => {
    setSelected([]);

    let newSelecteds = [];
    if (e.target.checked) {
      newSelecteds = data.map(v => v.id);
      setSelected(newSelecteds);
    }

    (typeof onChangeSelection === 'function') && onChangeSelection(newSelecteds);
  }

  const handleRowClick = (e, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);

    (typeof onChangeSelection === 'function') && onChangeSelection(newSelected);
  };

  const isSelected = id => selected.indexOf(id) !== -1;
  const rows = data.rows || [];

  return (
    <Fragment>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table size="small" aria-label="latest product price">
            <TableHead
              headCells={[
                { id: 'images.0', numeric: false, disablePadding: false, label: 'Image' },
                { id: 'name', numeric: false, disablePadding: false, label: 'Name' },
                { id: 'description', numeric: false, disablePadding: false, label: 'Description' },
                { id: 'latestPrice', numeric: false, disablePadding: false, label: 'Latest Price' },
              ]}
              rowCount={data.totalCount}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAll={handleSelectAll}
              onChangeOrder={onChangeOrder}
            />

            <TableBody>
              {rows.map((row, i)=> {
                const isItemSelected = isSelected(row._id);

                return (
                  <TableRow
                    hover
                    key={row._id}
                    onClick={e => handleRowClick(e, row._id)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    selected={isItemSelected}
                  >
                    <TableCell align="left" style={{ maxWidth: 200 }}>
                      <Avatar
                        alt={row._id}
                        src={row.images[0]}
                        variant="rounded"
                        className={clsx(classes.large, classes[i % 2 === 0 ? 'orange' : 'purple'])}
                      >
                        {row.name.split(' ').reduce((o, v) => o + v.slice(0, 1), '')}
                      </Avatar>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      <Link href="/products/[_id]" as={`/products/${row._id}`}>
                        <a>{row.name}</a>
                      </Link>
                    </TableCell>
                    <TableCell align="left" style={{ maxWidth: 200 }}>
                      {(() => {
                        const strippedHtml = row.description.replace(/(<([^>]+)>)/ig, '');

                        return strippedHtml.slice(0, 200);
                      })()} ...
                    </TableCell>
                    <TableCell align="left">
                      {(() => {
                        const formatted = (row.latestPrice || '').toLocaleString('en-ID', {
                          style: 'currency', currency: 'IDR'
                        });

                        return formatted;
                      })()}
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 50, 100]}
          component="div"
          count={data.totalCount || 0}
          rowsPerPage={rowsPerPage || 5}
          page={page || 0}
          onChangePage={onChangePage}
          onChangeRowsPerPage={onChangeRowsPerPage}
        />
      </Paper>
    </Fragment>
  );
}
