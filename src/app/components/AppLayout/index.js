import React, { Fragment } from 'react';
import NextLink from 'next/link';
import Head from 'next/head';
import Router from 'next/router'
import NProgress from 'nprogress';

import makeStyles from '@material-ui/core/styles/makeStyles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import ScrollTop from '../ScrollTop';

Router.events.on('routeChangeStart', (url) => { console.log(url); NProgress.start() });
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const useStyles = makeStyles((theme) => ({
  '@global': {
    '#nprogress .bar': {
      zIndex: 9000,
    },
  },
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  brand: {
    fontWeight: 'bold',
    [theme.breakpoints.down('sm')]: {
      flex: 1,
    }
  },
  navigationMenu: {
    marginLeft: theme.spacing(8),
  },
  toolbar: theme.mixins.toolbar,
}));

export default function AppLayout(props) {
  const { fullpage, showNav, children } = props;
  const classes = useStyles();

  return (
    <Fragment>
      <Head>
        {/* Import CSS for nprogress */}
        <link rel="stylesheet" type="text/css" href="https://unpkg.com/nprogress@0.2.0/nprogress.css" />
      </Head>
      <AppBar position="fixed">
        <Toolbar variant="dense">
          <Typography variant="h5" className={classes.brand}>
            <NextLink href="/" passHref>
              <Link color="inherit">Princey</Link>
            </NextLink>
          </Typography>
          {showNav && (
            <Box className={classes.navigationMenu}>
              <Typography variant="h6">
                <NextLink href="/products" passHref>
                  <Button variant="outlined" color="default">
                    Latest Product Price
                  </Button>
                </NextLink>
              </Typography>
            </Box>
          )}
        </Toolbar>
      </AppBar>
      {fullpage || <Toolbar id="back-to-top-anchor" />}
      {children}
      <ScrollTop {...props}>
        <Tooltip title="Back to Top" placement="left" arrow>
          <Fab color="secondary" size="large" aria-label="scroll back to top">
            <KeyboardArrowUpIcon />
          </Fab>
        </Tooltip>
      </ScrollTop>
    </Fragment>
  );
}
