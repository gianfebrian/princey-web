import React from 'react';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  box: {
    textAlign: 'center',
    lineHeight: 1.2,
  },
  text: {
    color: '#FFFFFF',
    fontSize: 28,
    fontWeight: 'bold',
  },
}));

export default function Slogan({ text }) {
  const classes = useStyles();

  return (
    <Box className={classes.box}>
      <Typography className={classes.text} component="span">{text}</Typography>
    </Box>
  );
}
