import React from 'react';

import RefreshIcon from '@material-ui/icons/Refresh';
import Button from '@material-ui/core/Button';

import PageToolbar from '../PageToolbar';

export default function ActionToolbar({
  title,
  subtitle,
  refresh = false,
  onRefresh = () => {},
  children,
}) {
  return (
    <PageToolbar title={title} substitle={subtitle}>
      {refresh && (
        <Button
          color="default"
          startIcon={<RefreshIcon />}
          onClick={onRefresh}
        >
          Refresh
        </Button>
      )}
      {children}
    </PageToolbar>
  );
}
