import React, { Fragment, useState, useEffect } from 'react';
import Head from 'next/head';

import { listProducts } from '../../services/api';
import ProductListContainer from '../../containers/ProductListContainer';
import AppLayout from '../../components/AppLayout';
import CrunchySnack from '../../components/CrunchySnack';

function ProductIndex({ data, error }) {
  return (
    <Fragment>
      <Head>
        <title>Pricing Monitor - List</title>
      </Head>
      <CrunchySnack error={error} />
      <AppLayout>
        <ProductListContainer data={data} />
      </AppLayout>
    </Fragment>
  );
}

ProductIndex.getInitialProps = async (req) => {
  try {
    const result = await listProducts(req.query);
    if (!result.ok) {
      throw new Error('internal server error');
    }

    const data = await result.json();

    return { data };
  } catch (error) {
    return { error, data: { rows: [], totalCount: 0 } };
  }
};

export default ProductIndex;
