import React, { Fragment } from 'react';
import Head from 'next/head';

import ProductDetailContainer from '../../containers/ProductDetailContainer';
import { getProducts } from '../../services/api';
import AppLayout from '../../components/AppLayout';
import CrunchySnack from '../../components/CrunchySnack';

function ProductById({ data, error }) {
  return (
    <Fragment>
      <Head>
        <title>Pricing Monitor - List</title>
      </Head>
      <AppLayout>
        <CrunchySnack error={error} />
        <ProductDetailContainer data={data} />
      </AppLayout>
    </Fragment>
  );
}

ProductById.getInitialProps = async (req) => {
  try {
    const result = await getProducts(req.query);
    if (!result.ok) {
      throw new Error('internal server error');
    }

    const data = await result.json();

    return { data };
  } catch (error) {
    return { data: { images: [] }, error };
  }
};

export default ProductById;
