import React, { Fragment, useState } from 'react';
import Head from 'next/head';
import clsx from 'clsx';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';

import InputMonitorContainer from '../containers/InputMonitorContainer';
import Brand from '../components/Brand';
import Slogan from '../components/Slogan';
import CallToAction from '../components/CallToAction';
import AppLayout from '../components/AppLayout';

const useStyles = makeStyles((theme) => ({
  '@global': {
    'html, body, body > div:first-child, div#__next, div#__next': {
      height: '100%',
      margin: 0,
    }
  },
  h100: {
    height: '100%',
  },
  background: {
    position: 'relative',
    background: 'url("https://firebasestorage.googleapis.com/v0/b/princey.appspot.com/o/bg.png?alt=media&token=bc00d74c-c870-4492-b8b1-bf8892bf7ee5") no-repeat center center',
    backgroundSize: 'cover',
  },
  mask: {
    backgroundColor: 'rgba(255, 255, 255, .55)',
  },
  root: {},
  container: {
    height: '75%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      overflow: 'scroll',
      height: '100%',
    },
    [theme.breakpoints.up('xs')]: {
      overflow: 'hidden',
      height: '100%',
    },
  },
  brand: {
    marginBottom: theme.spacing(3),
  },
  slogan: {
    marginBottom: theme.spacing(2),
  },
  callToAction: {
    marginBottom: theme.spacing(2),
  },
  inputMonitor: {
    width: '100%',
  },
}));

export default function Index() {
  const classes = useStyles();

  return (
    <Fragment>
      <Head>
        <title>Pricing Monitor - Home</title>
      </Head>
      <AppLayout showNav fullpage>
        <Box component="main" className={clsx(classes.root, classes.h100)}>
          <Box className={clsx(classes.background, classes.h100)}>
            <Box className={clsx(classes.mask, classes.h100)}>
              <Container className={clsx(classes.container, classes.h100)} fixed maxWidth="sm">
                <Box className={classes.brand}>
                  <Brand name="Princey" label="Price Monitor" />
                </Box>
                <Box className={classes.slogan}>
                  <Slogan text="Never Miss The Best Price" />
                </Box>
                <Box className={classes.callToAction}>
                  <CallToAction>
                    Let us know your wishlist product by submitting the product url.
                    We will check for any price change every hour
                    <strong> and notify you immediately!</strong>
                  </CallToAction>
                </Box>
                <Box className={classes.inputMonitor}>
                  <InputMonitorContainer />
                </Box>
              </Container>
            </Box>
          </Box>
        </Box>
      </AppLayout>
    </Fragment>
  );
}
