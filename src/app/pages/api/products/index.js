import Cors from 'micro-cors';

import constants from '../../../services/constants';
import client from '../../../services/client';

const products = `${constants.baseUrl}/products`;

const cors = Cors();

async function handler(req, res) {
  const result = await client({ url: products, query: req.query, method: 'get', });
  const data = await result.json();

  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(data));
}

export default cors(handler);
