
import Cors from 'micro-cors';

import constants from '../../../services/constants';
import client from '../../../services/client';

const schedulers = `${constants.baseUrl}/schedulers`;
const schedulersJobs = `${schedulers}/jobs`;
const schedulersStart = `${schedulers}/controls/start`;
const crawlers = `${constants.baseUrl}/crawlers`;
const crawlersCrawl = `${crawlers}/crawl`;
const products = `${constants.baseUrl}/products`;

const repeatInterval = '60 minutes';

const cors = Cors();

// specific for firebase
export const config = {
  api: {
    bodyParser: process.env.TARGET_ENV === 'next'
  }
};

async function handler(req, res) {
  if (req.method.toLowerCase() !== 'post') {
    return res.end();
  }

  const { url: productUrl } = req.body;
  const body = {
    repeatInterval,
    name: Date.now(),
    data: {
      url: crawlersCrawl,
      body: {
        url: productUrl,
        callback: {
          url: products,
        },
      },
    },
  };

  try {
    await client({ body, url: schedulersJobs });
    await client({ url: schedulersStart, method: 'get' })

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
  } catch (err) {
    res.statusCode = 400;
  } finally {
    res.end();
  }
}

export default cors(handler);
