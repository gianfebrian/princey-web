
const defaultTimeout = Number(process.env.DEFAULT_REQ_TIMEOUT || 0);

const baseUrl = process.env.BASE_API_URL;
const appHost = process.env.APP_HOST;

const monitors = `${appHost}/api/monitors`;
const monitorsStatus = `${monitors}/status`;
const products = `${appHost}/api/products`;

export default {
  defaultTimeout,
  appHost,
  baseUrl,
  monitors,
  monitorsStatus,
  products,
};
