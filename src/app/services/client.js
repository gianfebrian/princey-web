
import { URL } from 'whatwg-url';
import fetch from 'isomorphic-unfetch';

import constants from './constants';

export default function (data) {
  let dataUrl = data.url;
  if (data.params) {
    Object.entries(data.params)
      .forEach((p) => {
        dataUrl = dataUrl.replace(`:${p[0]}`, p[1]);
      });
  }

  const url = new URL(dataUrl);

  if (data.query) {
    Object.entries(data.query)
      .forEach((q) => url.searchParams.append(q[0], q[1]));
  }

  const options = {
    timeout: data.timeout || constants.defaultTimeout,
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  if (data.headers) {
    options.headers = { ...options.headers, ...data.headers };
  }

  if (data.method) {
    options.method = data.method.toLowerCase();
  }

  if (data.body) {
    options.body = JSON.stringify(data.body);
  }

  return fetch(url.toString(), options);
}
