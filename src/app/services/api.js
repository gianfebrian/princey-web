
import client from './client';
import constants from './constants';

export const postMonitor = (body) => (
  client({ url: constants.monitors, method: 'post', body })
);

export const getMonitorStatus = () => (
  client({ url: constants.monitorsStatus, method: 'get', data })
);

export const listProducts = (query) => (
  client({ url: constants.products, method: 'get', query })
);

export const getProducts = (params) => (
  client({ url: `${constants.products}/:_id`, method: 'get', params })
);
