import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import NextLink from 'next/link';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

import Html from '../../components/Html';
import PriceHistoryList from '../../components/PriceHistoryList';
import ActionToolbar from '../../components/ActionToolbar';

const useStyles = makeStyles((theme) => ({
  section: {
    marginTop: theme.spacing(2),
  },
  divider: {
    marginBottom: theme.spacing(2),
  },
  container: {},
  breadcrumbs: {
    marginBottom: theme.spacing(4),
  },
  grdiListBox: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  list: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
}));

export default function ProductDetailContainer({ data }) {
  const classes = useStyles();
  const router = useRouter();

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [sortBy, setSortBy] = useState('timestamp');
  const [sortDir, setSortDir] = useState('desc');
  const [priceHistory, setPriceHistory] = useState({ rows: [], totalCount: 0 });

  const sliceRows = (pg = page, rpg = rowsPerPage) => setPriceHistory({
    rows: (data.priceHistory || []).slice(pg * rpg, pg * rpg + rpg),
    totalCount: (data.priceHistory || []).length,
  });

  const sortRows = (key = sortBy, dir = sortDir) => {
    data.priceHistory = (data.priceHistory || []).sort((a, b) => (
      dir === 'asc' ? a[key] - b[key] : b[key] - a[key]
    ));
  };

  useEffect(() => {
    sortRows();
    sliceRows();
  }, [])

  const handleChangeOrder = (e, newSortBy) => {
    const isDesc = sortBy === newSortBy && sortDir === 'desc';
    const newSortDir = (sortBy !== newSortBy) || isDesc ? 'asc' : 'desc';

    sortRows(newSortBy, newSortDir);
    setSortBy(newSortBy);
    setSortDir(newSortDir);
    sliceRows();
  };

  const handleChangePage = (e, newPage) => {
    sliceRows(newPage);
    setPage(newPage);
  };

  const handleChangeRowsPage = (e) => {
    sliceRows(0, e.target.value);
    setRowsPerPage(e.target.value);
    setPage(0);
  };

  const handleClickRefresh = (e) => {
    router.push(router.pathname, router.asPath);
  };

  return (
    <Container maxWidth="md" className={classes.container}>
      <Breadcrumbs aria-label="breadcrumb" className={classes.breadcrumbs}>
        <NextLink href="/" passHref>
          <Link color="inherit">Home</Link>
        </NextLink>
        <NextLink href="/products" passHref>
          <Link color="inherit">Latest Product Price</Link>
        </NextLink>
        <Typography color="textPrimary">Product Detail</Typography>
      </Breadcrumbs>
      <ActionToolbar
        title={data.name || 'Product not found'}
        subtitle={
          (data.latestPrice || '').toLocaleString('en-ID', {
            style: 'currency', currency: 'IDR'
          })
        }
        refresh={!data.name}
        onRefresh={handleClickRefresh}
      />
      <Box className={classes.section}>
        <Typography variant="h6">Images:</Typography>
        <Divider className={classes.divider} />
        <Box className={classes.grdiListBox}>
          <GridList className={classes.gridList}>
            {(data.images || []).map((v, i) => (
              <GridListTile key={v}>
                <img src={v} alt={`${data._id}_i`} />
              </GridListTile>
            ))}
          </GridList>
        </Box>
      </Box>
      <Box className={classes.section}>
        <Typography variant="h6">Price History:</Typography>
        <Divider className={classes.divider} />
        <PriceHistoryList
          data={priceHistory}
          page={page}
          rowsPerPage={rowsPerPage}
          onChangeOrder={handleChangeOrder}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPage}
        />
      </Box>
      <Box className={classes.section}>
        <Typography variant="h6">Description:</Typography>
        <Divider className={classes.divider} />
        <Html html={data.description} />
      </Box>
    </Container>
  );
}
