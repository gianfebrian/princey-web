import React, { useState } from 'react';
import { useSnackbar } from 'notistack';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import VisibilityIcon from '@material-ui/icons/Visibility';

import { postMonitor } from './../../services/api';

const useStyles = makeStyles((theme) => ({
  urlInput: {
    width: '100%',
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    border: '4px solid #FED541',
  },
  input: {
    flex: 1,
    fontSize: 18,
    padding: theme.spacing(1),
  },
  buttonIcon: {
    marginRight: theme.spacing(1),
  },
  textIcon: {
    fontWeight: 'bold',
  },
}));

export default function InputMonitorContainer() {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [url, setUrl] = useState('');

  // hackable timer. don't rely on it. still works for most users thou
  const [timer, setTimer] = useState(0);

  const handleChangeUrl = (e) => setUrl(e.target.value);

  const handleClickMonitor = (e) => {
    const now = Date.now();
    const interval = now - timer;
    const allowIn = 5000 - interval;

    if (timer > 0 && interval <= 5 * 1000) { // request interval policy once every 5 seconds
      const inSecond = parseInt(allowIn / 1000);

      const messageABitLong = `Let's slow down a bit. Please wait for another ${inSecond} second(s)!`;
      const messageAlmost = `Right, one more moment... It should work now. Please try again!`;
      const message = inSecond ? messageABitLong : messageAlmost;
      const variant = inSecond ? 'warning' : 'info'

      return enqueueSnackbar(message, { variant });
    }

    setTimer(now);

    try {
      new URL(url);
    } catch (err) {
      setUrl('');
      return enqueueSnackbar(
        'We\'re sorry, it seems that you have provided an invalid url!', {
        variant: 'error',
      });
    }

    return postMonitor({ url })
      .then((res) => {
        if (res.ok) return res;
        throw new Error('internal server error');
      })
      .then(() => (
        enqueueSnackbar('Yeay! Your product price has been monitored by us!', {
          variant: 'success',
        })
      ))
      .then(() => setUrl(''))
      .catch(() => (
        enqueueSnackbar(
          'Duh! We fail to monitor your product price. Please try again!', {
          variant: 'error',
        })
      ));
  };

  return (
    <Paper className={classes.urlInput}>
      <InputBase
        placeholder="https://fabelio.com/ip/eton-square-dining-table.html"
        className={classes.input}
        onChange={handleChangeUrl}
        value={url}
      />
      <Button
        variant="contained"
        color="primary"
        onClick={handleClickMonitor}
      >
        <VisibilityIcon className={classes.buttonIcon} />
        <Typography className={classes.textIcon}>MONITOR</Typography>
      </Button>
    </Paper>
  )
}
