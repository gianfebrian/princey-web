import React from 'react';
import { useRouter } from 'next/router';
import NextLink from 'next/link';

import makeStyles from '@material-ui/core/styles/makeStyles';
import Container from '@material-ui/core/Container';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

import ProductList from '../../components/ProductList';
import ActionToolbar from '../../components/ActionToolbar';
import TextSearch from '../../components/TextSearch';

const useStyles = makeStyles((theme) => ({
  flexer: {
    flex: 1,
  },
  container: {
    maxHeight: 800,
  },
  breadcrumbs: {
    marginBottom: theme.spacing(4),
  }
}));

export default function ProductListContainer({ data }) {
  const classes = useStyles();
  const router = useRouter();
  const newRouter = { pathname: router.pathname, query: {} };

  const {
    sortBy = 'name',
    sortDir = 'asc',
    page = 1,
    rowsPerPage = 5,
    filter,
  } = router.query;


  const handleChangeOrder = (e, newSortBy) => {
    const isDesc = sortBy === newSortBy && sortDir === 'desc';
    const newSortDir = (sortBy !== newSortBy) || isDesc ? 'asc' : 'desc';

    const newSort = {
      sortBy: newSortBy,
      sortDir: newSortDir,
    };

    newRouter.query = { ...router.query, ...newSort };
    router.push(newRouter);
  };

  const handleChangePage = (e, newPage) => {
    newRouter.query = { ...router.query, page: newPage + 1 };
    router.push(newRouter);
  };

  const handleChangeRowsPerPage = (e) => {
    newRouter.query = { ...router.query, page: 1, rowsPerPage: e.target.value };
    router.push(newRouter);
  };

  const handleChangeSearch = (e) => {
    const criteria = e.target.value;
    const { filter: prevFilter } = router.query;

    const newRouter = {
      pathname: router.pathname,
      query: {},
    };

    if (criteria.length < 4) {
      if (prevFilter) {
        return router.push(newRouter);
      }

      return null;
    }

    newRouter.query.filter = criteria;

    return router.push(newRouter);
  };

  const handleClickRefresh = () => (
    router.push({ ...newRouter, query: { ...router.query } })
  );

  return (
    <Container maxWidth="md" className={classes.container}>
      <Breadcrumbs aria-label="breadcrumb" className={classes.breadcrumbs}>
        <NextLink href="/" passHref>
          <Link color="inherit">Home</Link>
        </NextLink>
        <Typography color="textPrimary">Latest Product Price</Typography>
      </Breadcrumbs>
      <ActionToolbar
        title="Latest Product Price"
        refresh
        onRefresh={handleClickRefresh}
        search
      >
        {/* <div className={classes.flexer} /> */}
        <TextSearch
          onChange={handleChangeSearch}
          defaultValue={filter}
        />
      </ActionToolbar>
      <ProductList
        data={data}
        order={sortDir}
        orderBy={sortBy}
        page={page - 1}
        rowsPerPage={Number(rowsPerPage)}
        onChangeOrder={handleChangeOrder}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Container>
  );
}
