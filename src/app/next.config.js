require('dotenv').config();

const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withBundleAnalyzer({
  env: {
    APP_ENV: process.env.APP_ENV,
    APP_VERSION: process.env.npm_package_version,
    APP_HOST: process.env.APP_HOST,
    BASE_API_URL: process.env.BASE_API_URL,
    DEFAULT_REQ_TIMEOUT: process.env.DEFAULT_REQ_TIMEOUT,
  },
});
